package com.example.ander.repository.impl;
import com.example.ander.domain.Producto;
import com.example.ander.repository.ProductoRepository;
import com.example.ander.repository.mapper.ProductoMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProductoRepositoryImpl implements ProductoRepository {

    private static final Log LOG = LogFactory.getLog(ProductoRepositoryImpl.class);
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductoRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public void insert(Producto producto) {
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(this.jdbcTemplate.getDataSource())
                .withProcedureName("insert_product");
        Map<String, Object> parameters = new HashMap<>();
//        parameters.put("id",producto.getId());
        LOG.info("ProductoRepositoryImpl - insert() ");
        parameters.put("_codigo",producto.getCodigo());
        parameters.put("_nombre",producto.getNombre());
        parameters.put("_descripcion",producto.getDescripcion());
        parameters.put("_estado",producto.getEstado());
        simpleJdbcCall.execute(parameters);
        /*
        LOG.info("Registrando:");
        LOG.info("id: " + producto.getId());
        LOG.info("codigo: " + producto.getCodigo());
        LOG.info("descrip: " + producto.getDescripcion());
        LOG.info("Estado: " +  producto.getEstado());
        LOG.info("nombre: " + producto.getNombre());
        String sql = "insert into producto (codigo,descripcion,estado,nombre) values('" + producto.getCodigo() + "','"+ producto.getDescripcion() + "'," + producto.getEstado() + ",'" + producto.getNombre() + "');";
        LOG.info("query: " + sql);
        this.jdbcTemplate.execute(sql);
        LOG.info("Registro OK");
         */
    }

    @Override
    public void update(Producto producto, long id) {
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(this.jdbcTemplate.getDataSource())
                .withProcedureName("update_product");
        Map<String, Object> parameters = new HashMap<>();
        LOG.info("ProductoRepositoryImpl -  update(Producto producto) ");
        parameters.put("_codigo",producto.getCodigo());
        parameters.put("_nombre",producto.getNombre());
        parameters.put("_descripcion",producto.getDescripcion());
        parameters.put("_estado",producto.getEstado());
        parameters.put("_id",id);
        simpleJdbcCall.execute(parameters);
    }

    @Override
    public void delete(long id) {
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(this.jdbcTemplate.getDataSource())
                .withProcedureName("eliminar_producto");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("_id",id);
        simpleJdbcCall.execute(parameters);

        /*
        String sql="delete from producto where id = "+id+";";
        LOG.info("query: "+sql );
        this.jdbcTemplate.execute(sql);
        LOG.info("execute Ok , Eliminado Correctamente");

         */
    }

    @Override
    public List<Producto> findAll() {
        LOG.info("En  ProductoRepositoryImpl  --> findAll() ");
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(this.jdbcTemplate.getDataSource())
                .withProcedureName("listar_productos")
                .returningResultSet("result", new ProductoMapper());
        Map<String, Object> parameters = new HashMap<>();

        Map<String, Object> result = simpleJdbcCall.execute(parameters);

        List<Producto> productos = new ArrayList<>();
        productos = (List<Producto>) result.get("result");
        return productos;

        /*
        String sql = "select * from producto;";
        List datos = this.jdbcTemplate.queryForList(sql);
        return datos;
         */
    }

    @Override
    public List<Producto> FindByName(String nombre) {
        String sql ="select * from producto where nombre like '%"+nombre+"%'";
        LOG.info("Producto Repository-Impl FindByName");
        LOG.info("query: " +sql);
        List datos = this.jdbcTemplate.queryForList(sql);
        return datos;
    }




    @Override
      public boolean buscarByIdBoolean( long id) {
        LOG.info("Evaluando ID: ProductoRepositoryImpl  buscarByIdBoolean ");
        boolean valorDelaVerdad = true;
        String sql = "SELECT * FROM producto where id =" + id + ";";
        LOG.info("query: " + sql);
        List datos = this.jdbcTemplate.queryForList(sql);
        LOG.info(datos);

        if (datos.isEmpty()) {
            valorDelaVerdad = false;
        } else {
            valorDelaVerdad = true;
        }
        return valorDelaVerdad;
    }

    @Override
    public boolean buscarByNameBoolean(String nombre) {
        boolean valorDelaVerdad = true;
        LOG.info("String nombre : " + nombre);
        String sql = "SELECT * FROM producto where nombre ='" + nombre + "';";
        LOG.info("query: " + sql);
        List datos = this.jdbcTemplate.queryForList(sql);
        LOG.info(datos);
        if (datos.isEmpty()) {
            valorDelaVerdad = true;
        } else {
            valorDelaVerdad = false;
        }
        return valorDelaVerdad;
    }


}
