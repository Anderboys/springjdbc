package com.example.ander.request;

import lombok.Data;

@Data
public class Producto {

    private long id;
    private String codigo;
    private String nombre;
    private String descripcion;
    private Boolean estado;
}
